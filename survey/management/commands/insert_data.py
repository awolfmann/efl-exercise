import json
from django.core.management.base import BaseCommand

import json
from survey.models import *

class Command(BaseCommand):
    help = "Command for insert test data"

    def handle(self, *args, **options):
        with open('payload.json') as json_file:
            json_data = json.load(json_file)
            for survey in json_data:
                section = survey['step']  # Simple Field transformation
                for observation, values in survey['observations'].items():
                    questionCode = observation  # Simple Field transformation
                    simple_fields = set(['responseSequence','responseValue', 'viewCount', 'timeElapsed'])
                    others = set(values.keys()) - simple_fields  # Complex fields transformation

                    if 'responseValue' in values:  # Simple Field transformation
                        questionResponse = values['responseValue']
                        if len(others) != 0:
                            # If 'responseValue' is in the observation, there are
                            # not other complex fields to transform
                            raise ValueError
                    if 'responseSequence' in values:  # Simple Field transformation
                        responseSequence = values['responseSequence']
                    if 'viewCount' in values:  # Simple Field transformation
                        viewsCount = values['viewCount']
                    if 'timeElapsed' in values:  # Simple Field transformation
                        timeElapsed = values['timeElapsed']

                    
                    for other in others:
                        # assuming there is no 'responseValue' field, 
                        # the complex fields are inserted in questionResponse field 
                         questionResponse =  other + ' ' + str(values[other])
                    a = assessmentResponse(
                            section=section,
                            timeElapsed=timeElapsed,
                            questionResponse=questionResponse,
                            questionCode=questionCode,
                            viewsCount=viewsCount,
                            responseSequence=responseSequence
                        )
                    a.save()

                for meta, meta_values in survey['metas'].items():
                    questionCode = '_' + section + ' ' + meta
                    timeElapsed = None
                    viewsCount = None
                    questionResponse = None
                    # There are some simple meta fields to transform
                    if meta == 'timeElapsed':
                        timeElapsed = meta_values
                    elif meta == 'viewCount':
                        viewsCount = meta_values
                    else:  # Complex fields
                        questionResponse = meta + ' ' + str(meta_values)

                    a = assessmentResponse(
                            section=section,
                            timeElapsed=timeElapsed,
                            questionResponse=questionResponse,
                            questionCode=questionCode,
                            viewsCount=viewsCount,
                            responseSequence=responseSequence
                        )
                    a.save()