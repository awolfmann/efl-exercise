from django.db import models


class assessmentResponse(models.Model):
    questionCode = models.CharField(max_length=104, null=True)
    questionResponse = models.CharField(max_length=104, null=True)
    section = models.CharField(max_length=104, null=True)
    timeElapsed = models.SmallIntegerField(null=True)
    viewsCount = models.SmallIntegerField(null=True)
    responseSequence = models.CharField(max_length=104, null=True)
