# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='assessmentResponse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('questionCode', models.CharField(max_length=104, null=True)),
                ('questionResponse', models.CharField(max_length=104, null=True)),
                ('section', models.CharField(max_length=104, null=True)),
                ('timeElapsed', models.SmallIntegerField(null=True)),
                ('viewsCount', models.SmallIntegerField(null=True)),
                ('responseSequence', models.CharField(max_length=104, null=True)),
            ],
        ),
    ]
